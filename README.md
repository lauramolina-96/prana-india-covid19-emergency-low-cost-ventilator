**Need of a Low Cost Ventilator, Save Prāṇa**

**Prāṇa**, (Sanskrit: “breath”) in Indian philosophy, the body’s vital “airs,” or energies. COVID-19 is eliminating lives or **Prāṇa** by impacting lungs causing issues to breathe (**Prāṇa**). India has 2.4 ICU beds for every 100,000 in comparison Italy has 12.5. [Read more details here - Statista](https://www.statista.com/chart/21105/number-of-critical-care-beds-per-100000-inhabitants/). Based on conversations with few Docters in India, UK and USA, we in India could have a 8:1 Ventilator shortage (8 patients waiting for one ventilator) atleast if the COVID-19 outbreak happens in India similar to Italy and China. These numbers are estimates only and need further validation by the Govt and other official sources. 

We need your help to consolidate product design requirements (speak to anesthesiologists), secure Ambu bag supply chain in a organized manner and low cost, contribute to mechanical concepts and Embedded design with our team. We will need funding for 3D printing, buying electronics to make this kit, once we have more clarity and finalize the design



**Product Design Objectives**

Design a low cost ventilator to automate the manual compression action of a standard AMBU bag or BAIN Circuit is planned. 


1. **Mechanical requirements** - The design needs to be sturdy to house a AMBU bag or BAIN Circuit and the movement of pressure plates, along with housing the motor, power supply, ambu bag firmly in its location and be able to move it easily across ICU beds. The mechanical design needs to have least number of parts which can be lazer cut and assembled together.

2. **Electronics / Embedded** - Adrino based designs to manage Ventilator Controls like Breaths Per Min and Tidal Volume is needed. In addition choice of right motor for long & reliabile performance along with Power supply to manage voltage fluctuation and power outage is needed.

3. **Supply Chain** - Ambu bags are available between Rs.440 - Rs.800 in India. However in the current scenario this cost could increase significantly due to increase in demard. We need to secure supply chain once the design is finalized to ensure we dont hoard the essential component, yet this is available to this project.

**COVID19 Medical Protocols**
1. [WHO Protocol for COVID, Released on Mar 13, 2020](https://www.who.int/docs/default-source/coronaviruse/clinical-management-of-novel-cov.pdf?sfvrsn=bc7da517_10&download=true)
2. European Society of Anaesthesiology (ESA) -[Airway Management in patients suffering from COVID-19, Released on March 11, 2020](https://www.esahq.org/esa-news/covid-19-airway-management/)
2. [United Kingdom](https://www.brit-thoracic.org.uk/about-us/covid-19-information-for-the-respiratory-community/)
3. United States 
    *  [Centers for Disease Control and Prevention](https://www.cdc.gov/coronavirus/2019-ncov/hcp/clinical-guidance-management-patients.html)
    *  [Saint John Regional Department of Emergency Medicine](http://sjrhem.ca/covid-19-clinical-management/)
    *  [Newyork COVID Protocol, March 14, 2020](https://www.cuimc.columbia.edu/file/44538/download?token=luHx0iV_)
4. [Medical Journal of Australia, March 16, 2020](https://www.mja.com.au/journal/2020/212/10/consensus-statement-safe-airway-society-principles-airway-management-and)
5. China

**Ventilator Basics**
1.  Ambu bag basics - [Manual Resuscitators by Saneesh & team from AnesthesiaTOOLS](https://www.youtube.com/watch?v=Sj_qDlc4BfM&t=800s)
2.  Ventilation in ARDS scenario - [This Youtube describes ventilation in ARDS scenario](https://youtu.be/KHpJ21UWbhg?t=330)
3.  COVID-19 | Corona Virus: Epidemiology, Pathophysiology, Diagnostics [Youtube](https://www.youtube.com/watch?v=PWzbArPgo-o)
4.  COVID19 (Corona Virus) Intubation Packs and Preoxygenation [Youtube](https://www.youtube.com/watch?v=C78VTEAHhWU)

**COVID19 Data Visualisation**
*  Covid-19-coronavirus-infographic-datapack [Link](https://informationisbeautiful.net/visualizations/covid-19-coronavirus-infographic-datapack/)
*  [NextStrain](https://nextstrain.org/ncov) - NextStrain builds tools to visualize the evolution and spread of the virus in real-time specifically for virologists, epidemiologists, public health officials, and community scientists.
*  [Other leading data visualisations](https://blog.mapbox.com/notable-maps-visualizing-covid-19-and-surrounding-impacts-951724cc4bd8)

**Additional information**

On March 13, 2020, Society of critical care medicine in the US has released a report on the number of ventilators which are needed. important points are below. Detailed report can be read here
* American Hospital Association (AHA) has reportedly predicted that if the COVID-19 virus continues to spread as projected, 4.8 million patients would need hospitalization, 1.9 million COVID-19 patients would require admission to ICUs and 960,000 would require mechanical ventilation.
* shortage of ICU physicians and support staff trained in mechanical ventilation could limit the maximum number of ventilated patients to approximately 135,000 against projected 1.9 Million according to the SCCM, that's about five patients for each ventilator that could be put into service
* SCCM Ventilator Task Force estimated that some 62,000 fully functional mechanical ventilators are in operation in the U.S., with roughly 100,000 older models that could be put in use if needed, including 22,976 noninvasive ventilators, 32,668 automatic resuscitators, and 8,567 CPAP units
* U.S. hospitals could absorb between 26,000 and 56,000 additional ventilators at the peak of a national pandemic, as safe use of ventilators requires trained personnel